const mongoose = require('mongoose');
const log = require('./yogi_app/logs/logger');

const get_line_no = () => {
    var obj = {};
    Error.captureStackTrace(obj, get_line_no);
    return obj.stack;
};

module.exports.connect_db = () => {
    try{
        mongoose.connect('mongodb://localhost:27017/yogi_db');
        mongoose.Promise = require('bluebird');
    }
    catch(err){
        log.error(err,get_line_no());
        console.log(err);
    }
};
