import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import QuestionAnswerSharpIcon from '@material-ui/icons/QuestionAnswerSharp';
import { withRouter } from "react-router-dom";

import { EnvProp, Headers } from '../services/envProperties';
import './chat.css';

class Chat extends React.Component {
    state = {
        msgTyped: '',
        msgList: [],
        chatHidden: true
    };

    pingCheck = async () => {
        try {
            console.log('check 123');
            let obj = {};
            let config = Headers(obj);
            let response = await fetch(EnvProp.baseUrl + '/pingCheck', config);
            let data = await response.json();
            let tempList = [...this.state.msgList];
            tempList.push(data);
            this.setState({ msgList: tempList });
        }
        catch (err) {
            console.log(err);
        }
    }

    sendMsg = async () => {
        try {
            let obj = {
                'msg': this.state.msgTyped
            }
            let config = Headers(obj);
            let response = await fetch(EnvProp.baseUrl + '/testIt', config);
            let data = await response.json();
            console.log(data);
            let tempList = [...this.state.msgList];
            tempList.push(this.state.msgTyped);
            this.setState({ msgList: tempList, msgTyped: '' });
            setInterval(this.pingCheck, 5000);
        }
        catch (err) {
            console.log(err);
        }
    }

    openChat = () => {
        this.setState({ chatHidden: false });
    }

    render() {
        return (
            <Grid container xs={12}>
                <div hidden={this.state.chatHidden}>
                    <Grid container xs={12}>
                        <Grid container xs={12}>
                            {this.state.msgList.map((e) => {
                                return (
                                    <Grid container xs={12}>
                                        <span>{e}</span>
                                    </Grid>
                                )
                            })}
                        </Grid>
                        <Grid container xs={12}>
                            <TextField onChange={(event) => this.setState({ msgTyped: event.target.value })}
                                label="Type your msg" type="text" value={this.state.msgTyped} required={true} />
                            <Button onClick={() => this.sendMsg()}>Send</Button>
                        </Grid>
                    </Grid>
                </div>
                <div hidden={!this.state.chatHidden} className="hand">
                    <QuestionAnswerSharpIcon color="primary" style={{ height: 40, width: 40, color: '888'}} onClick={() => this.openChat()} />
                </div>
            </Grid>
        )
    }
}

export default withRouter(Chat);