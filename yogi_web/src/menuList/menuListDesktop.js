import React from 'react';
import Grid from '@material-ui/core/Grid';
import FacebookIcon from '@material-ui/icons/Facebook';
import YouTubeIcon from '@material-ui/icons/YouTube';
import InstagramIcon from '@material-ui/icons/Instagram';
import { withRouter } from "react-router-dom";

import { EnvProp } from '../services/envProperties';

import './menuList.css';

const menuItems = [
    { name: 'PRE WEDDING', subMenu: [], path: '/preWedding' },
    { name: 'WEDDING', subMenu: [], path: '/wedding' },
    { name: 'ALBUM', path: '', subMenu: [{ name: 'PRE WEDDING', path: '/preWedding' }, { name: 'WEDDING', path: '/wedding' }] },
    //{ name: 'WALL ART', subMenu: [], path: '/backtest/create' },
    { name: 'VIDEO', subMenu: [], path: '/video' },
    { name: '', subMenu: [], path: '' },
    { name: 'ABOUT YOGIPHOTOART', subMenu: [], path: '/aboutUs' },
    { name: 'MY PROMISE', subMenu: [], path: '/myPromise' },
    { name: 'TESTIMONIALS', subMenu: [], path: '/testimonials', state: 'testimonials' },
    { name: 'FAQ', subMenu: [], path: '/help' },
    { name: 'BLOGS', subMenu: [], path: '/blogs' },
    { name: '', subMenu: [], path: '' },
    { name: 'CONTACT', subMenu: [], path: '/contact' },
    { name: 'HELLO@YOGIPHOTOART.COM', subMenu: [], path: '' },
    { name: '(+91) 955 255 7989', subMenu: [], path: '' },
];

class MenuListDesktop extends React.Component {

    state = {
        yogiLogoUrl: EnvProp.webImgUrl + 'yogi_logo.png',
        arrowRight: EnvProp.webImgUrl + 'arrow.svg'
    }

    goToPath = (e) => {
        if('' !== e.path){
            this.props.history.push(e.path);
        }
    }

    goToHome = () => {
        this.props.history.push('/');
    }

    goToFb = () => {
        window.open('https://www.facebook.com/yogiphotoart', '_blank');
    }

    goToInsta = () => {
        window.open('https://www.instagram.com/yogiphotoart/', '_blank');
    }

    goToYoutube = () => {
        window.open('https://www.youtube.com/channel/UCxsE7aU5vuPBlhY8_I0HF4g', '_blank');
    }

    render() {
        return (
            <div className="menuDeskWidth">
                <Grid container xs={12} justify="center">
                    <img alt='' onClick={() => this.goToHome()} className="yogiLogo hand" src={this.state.yogiLogoUrl} />
                </Grid>
                <div>
                    {menuItems.map((e) => {
                        return (
                            <div>
                                <Grid container xs={12} className="menuFont textAlignEnd">
                                    <Grid xs={10}>
                                        <div className="showhim">
                                            <span className="hand" onClick={() => this.goToPath(e)}>
                                                {e.name}
                                                <img src={this.state.arrowRight} className="arrowRight" alt='arrow' hidden={'ALBUM' !== e.name} />
                                            </span>
                                            <div className="showme">
                                                <div hidden={'ALBUM' !== e.name} className="menuExtended">
                                                    {e.subMenu.map((i, idx, arr) => {
                                                        return (
                                                            <div className={(idx === (arr.length - 1)) ? 'mTopExtMenu' : 'placeboClass'}>
                                                                <div className="hand">
                                                                    <span onClick={() => this.goToPath(i)}>{i.name}</span>
                                                                </div>
                                                            </div>
                                                        )
                                                    })}
                                                </div>
                                            </div>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                        )
                    })}
                </div>
                <div>
                    <Grid container xs={12}>
                        <div className="fbIcon menuFont" onClick={() => this.goToFb()}>
                            <FacebookIcon />
                        </div>
                        <div className="mRL10 menuFont" onClick={() => this.goToInsta()}>
                            <InstagramIcon />
                        </div>
                        <div className="mRL10 menuFont" onClick={() => this.goToYoutube()}>
                            <YouTubeIcon />
                        </div>
                    </Grid>
                </div>
            </div>
        )
    }
}

export default withRouter(MenuListDesktop);