import React from 'react';
import Grid from '@material-ui/core/Grid';
import FacebookIcon from '@material-ui/icons/Facebook';
import YouTubeIcon from '@material-ui/icons/YouTube';
import InstagramIcon from '@material-ui/icons/Instagram';
import { withRouter } from "react-router-dom";

import { EnvProp } from '../services/envProperties';


const menuItems = [
    { name: 'PRE WEDDING', subMenu: [], path: '/preWedding', showSubMenu: false },
    { name: 'WEDDING', subMenu: [], path: '/wedding' },
    { name: 'ALBUM', path: '', subMenu: [{ name: 'PRE WEDDING', path: '/preWedding' }, { name: 'WEDDING', path: '/wedding' }], showSubMenu: false },
    //{ name: 'WALL ART', subMenu: [], path: '/backtest/create', showSubMenu: false },
    { name: 'VIDEO', subMenu: [], path: 'video', showSubMenu: false },
    { name: 'ABOUT YOGIPHOTOART', subMenu: [], path: '/aboutUs', showSubMenu: false },
    { name: 'MY PROMISE', subMenu: [], path: '/myPromise', showSubMenu: false },
    { name: 'TESTIMONIALS', subMenu: [], path: '/testimonials', state: 'private', showSubMenu: false },
    { name: 'FAQ', subMenu: [], path: '/help', showSubMenu: false },
    { name: 'BLOGS', subMenu: [], path: '/blogs', showSubMenu: false },
    { name: 'CONTACT', subMenu: [], path: '/contact', showSubMenu: false },
    { name: 'HELLO@YOGIPHOTOART.COM', subMenu: [], path: '', showSubMenu: false },
    { name: '(+91) 955 255 7989', subMenu: [], path: '', showSubMenu: false },
];

class MenuList extends React.Component {

    state = {
        arrowRight: EnvProp.webImgUrl + 'arrow.svg',
        showSubMenu: false
    }

    goToHome = () => {
        this.props.history.push('/');
    }

    goToMenu = (value) => {
        if (0 === value.subMenu.length) {
            this.props.history.push(value.path);
            this.props.closeMenu(false);
        }
        else {
            menuItems.forEach((e) => {
                if (value.name === e.name) {
                    e.showSubMenu = true;
                    this.setState({ showSubMenu: !this.state.showSubMenu });
                }
            })
        }
    }

    goToSubMenu = (value) => {
        this.props.history.push(value.path);
        this.props.closeMenu(false);
    }

    goToFb = () => {
        window.open('https://www.facebook.com/yogiphotoart', '_blank');
    }

    goToInsta = () => {
        window.open('https://www.instagram.com/yogiphotoart/', '_blank');
    }

    goToYoutube = () => {
        window.open('https://www.youtube.com/channel/UCxsE7aU5vuPBlhY8_I0HF4g', '_blank');
    }

    render() {
        return (
            <Grid container>
                <div>
                    {menuItems.map((e) => {
                        return (
                            <div className="hambMenu" key={e.name}>
                                <Grid key={e.name} component="nav" aria-label="main mailbox folders">
                                    <div button onClick={() => this.goToMenu(e)}>
                                        <span className="hand" onClick={() => this.goToMenu(e)}>
                                            {e.name}
                                            <img src={this.state.arrowRight} className="arrowMobile" alt='arrow' hidden={'ALBUM' !== e.name} />
                                        </span>
                                    </div>
                                    <div>
                                        {e.subMenu.map((i, idx, arr) => {
                                            return (
                                                <div className="hand subMenuMobile" hidden={!e.showSubMenu}>
                                                    <span onClick={() => this.goToSubMenu(i)}>{i.name}</span>
                                                </div>
                                            )
                                        })}
                                    </div>
                                </Grid>
                            </div>
                        )
                    })}
                </div>
                <div>
                    <Grid container xs={12}>
                        <div className="fbIcon menuFont" onClick={() => this.goToFb()}>
                            <FacebookIcon />
                        </div>
                        <div className="mRL10 menuFont" onClick={() => this.goToInsta()}>
                            <InstagramIcon />
                        </div>
                        <div className="mRL10 menuFont" onClick={() => this.goToYoutube()}>
                            <YouTubeIcon />
                        </div>
                    </Grid>
                </div>
            </Grid>
        )
    }
}

export default withRouter(MenuList);