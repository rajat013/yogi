import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withRouter } from "react-router-dom";

import { EnvProp } from '../services/envProperties';
import { HomeImgList } from '../tiles/tiles';
import { HomeContent } from '../content/content'
import './home.css';

class Home extends React.Component {

    state = {

    };

    componentDidMount() {
        console.log(HomeImgList);
        window.scroll(0,0);
    }

    goToPath = (path) => {
        this.props.history.push(path);
    }

    render() {
        return (
            <div className="homeBody">
                <Grid container xs={12}>
                    <div className="margin5 imgContainer hand" onClick={() => this.goToPath(HomeImgList[0].path)}>
                        <img src={HomeImgList[0].img} className="imgWidth" alt='' />
                        <div className="bottomCentre">
                            <span>{HomeImgList[0].author}</span>
                        </div>
                    </div>
                </Grid>
                <Grid container xs={12}>
                    <Grid container xs={4}>
                        <div className="margin5 imgContainer hand" onClick={() => this.goToPath(HomeImgList[1].path)}>
                            <img src={HomeImgList[1].img} className="imgWidth" alt='' />
                            <div className="bottomCentre">{HomeImgList[1].author}</div>
                        </div>
                    </Grid>
                    <Grid container xs={4}>
                        <div className="margin5 imgContainer hand" onClick={() => this.goToPath(HomeImgList[2].path)}>
                            <img src={HomeImgList[2].img} className="imgWidth" alt='' />
                            <div className="bottomCentre">{HomeImgList[2].author}</div>
                        </div>
                    </Grid>
                    <Grid container xs={4}>
                        <div className="margin5 imgContainer hand" onClick={() => this.goToPath(HomeImgList[3].path)}>
                            <img src={HomeImgList[3].img} className="imgWidth" alt='' />
                            <div className="bottomCentre">{HomeImgList[3].author}</div>
                        </div>
                    </Grid>
                </Grid>
                <Grid container xs={12}>
                    <Grid container xs={6}>
                        <div className="margin5 imgContainer hand" onClick={() => this.goToPath(HomeImgList[4].path)}>
                            <img src={HomeImgList[4].img} className="imgWidth" alt='' />
                            <div className="bottomCentre">{HomeImgList[4].author}</div>
                        </div>
                    </Grid>
                    <Grid container xs={6}>
                        <div className="margin5 imgContainer hand" onClick={() => this.goToPath(HomeImgList[5].path)}>
                            <img src={HomeImgList[5].img} className="imgWidth" alt='' />
                            <div className="bottomCentre">{HomeImgList[5].author}</div>
                        </div>
                    </Grid>
                </Grid>
                <Grid container xs={12} className="pLeftRight15">
                    <Grid container xs={12} justify="center">
                        <h1 className="h1Text h1Mtop">DESTINATION WEDDING PHOTOGRAPHY</h1>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <img src={EnvProp.homeImgUrl + '/home_1.jpg'} className="contentImg" alt='' />
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_1}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_2}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_3}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center" className="contentSubHeader">
                        <h2>Elopement Photographer in Nagpur</h2>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <img src={EnvProp.homeImgUrl + '/home_2.jpg'} className="contentImg" alt='' />
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_4}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_5}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_6}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center" className="contentSubHeader">
                        <h2>Custom Album Design</h2>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <img src={EnvProp.homeImgUrl + '/home_3.jpg'} className="contentImg" alt='' />
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_7}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center" className="contentSubHeader">
                        <h2>Pre wedding photos</h2>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <img src={EnvProp.homeImgUrl + '/home_4.jpg'} className="contentImg" alt='' />
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_8}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_9}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_10}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_11}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center" className="contentSubHeader">
                        <h2>About Sushil</h2>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_12}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_13}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_14}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{HomeContent.home_15}</p>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(Home);