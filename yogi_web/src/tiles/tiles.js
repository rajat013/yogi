import { EnvProp } from '../services/envProperties';

export const HomeImgList = [{
  img: EnvProp.homeImgUrl + '/yogi.jpg',
  title: 'yogi_img',
  author: 'WEDDING GALLERY',
  path: '/wedding',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.homeImgUrl + '/yogi_1.jpg',
  title: 'yogi_img',
  author: 'PRE WEDDING GALLERY',
  path: '/preWedding',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.homeImgUrl + '/yogi_2.jpg',
  title: 'yogi_img',
  author: 'WEDDING ALBUM',
  path: '/wedding',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.homeImgUrl + '/yogi_3.jpg',
  title: 'yogi_img',
  author: 'PRE WEDDING ALBUM',
  path: '/preWedding',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.homeImgUrl + '/yogi_4.jpg',
  title: 'yogi_img',
  author: 'WALL ART',
  path: '/wallArt',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.homeImgUrl + '/yogi_5.jpg',
  title: 'yogi_img',
  author: 'ABOUT YOGIPHOTOART',
  path: '/aboutUs',
  type: 'P',
  height: '500px',
  classHeight: '500'
}]


//wedding image list
export const WeddingList = [{
  img: EnvProp.weddingImgUrl + '/yogi_img (1).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (2).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (3).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (4).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (5).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (6).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (7).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (8).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (9).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (10).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (11).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (12).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (13).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (14).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (15).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (16).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (17).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (18).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (19).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (20).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (21).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (22).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (23).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (24).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (25).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (26).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (27).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (28).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (29).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (30).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (31).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (32).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (33).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (34).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (35).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (36).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (37).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.weddingImgUrl + '/yogi_img (38).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
}];


///pre wedding image list
export const PreWeddingList = [{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (1).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (2).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (3).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (4).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (5).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (6).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (7).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (8).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (9).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (10).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (11).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (12).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (13).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (14).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (15).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (16).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (17).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'L',
  height: '250px',
  classHeight: '250'
},
{
  img: EnvProp.preWeddingImgUrl + '/yogi_img (18).jpg',
  title: 'yogi_img',
  author: 'author',
  type: 'P',
  height: '500px',
  classHeight: '500'
}];