import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withRouter } from "react-router-dom";

import { EnvProp } from '../services/envProperties';
import { AboutUsContent } from '../content/content'
import '../home/home.css';

class AboutUs extends React.Component {

    state = {

    };

    componentDidMount() {
        window.scroll(0, 0);
    }

    goToPath = (path) => {
        this.props.history.push(path);
    }

    render() {
        return (
            <div className="homeBody">
                <Grid container xs={12} className="pLeftRight15">
                    <Grid container xs={12} justify="center">
                        <img src={EnvProp.homeImgUrl + '/yogi_5.jpg'} style={{width:'40%'}} alt='' />
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <h1 className="h1Text h1Mtop">About Sushil</h1>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_1}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_2}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_3}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_4}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center" className="contentSubHeader">
                        <h2>ABOUT YOGI PHOTOART</h2>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_5}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_6}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_7}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center" className="contentSubHeader">
                        <h2>Style</h2>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_8}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_9}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{AboutUsContent.aboutUs_10}</p>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(AboutUs);