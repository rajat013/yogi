export const EnvProp = {
    //baseUrl: 'https://www.yogiphotoart.com/api', 15.207.86.147
    baseUrl: '/api',
    //baseUrl: 'http://192.168.1.103:4000/api',
    //baseUrl: 'http://192.168.43.52:4000/api',

    preWeddingImgUrl: '/preWedding',
    weddingImgUrl: '/wedding',
    homeImgUrl: '/home',
    aboutUsImgUrl: '/aboutUs',
    webImgUrl: '/',
};

export const Headers = (obj) => {
    let config = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(obj)
    }
    return config;
}

// [and,or,and,or,and,and,or]
// or,and,or,and,and,or

// 1st or 2nd - A
// 3rd or 4th - B
// 5th and 6th - C
// or 7th - D

// A and B and C or D

// [and,or,and,or,and,and,or]
// [true, false] = true - [false, false] = false
// [true, true] = true - [false, true] = false
// [true, false] = true - [false, true = true]


// (A or B) and (C or D) and (E and F)

// and,and,or,and,or,or

// (A) and (B) or ((C) and (D or E or F))

// and
// cond1
// and 
// cond2 
// or 
// cond3
// and
// cond4
// or
// cond5
// or
// cond6


