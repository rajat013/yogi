import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withRouter } from "react-router-dom";

import { TestimonialsContent } from '../content/content'
import '../home/home.css';

class Testimonials extends React.Component {

    state = {

    };

    componentDidMount() {
        window.scroll(0, 0);
    }

    goToPath = (path) => {
        this.props.history.push(path);
    }

    render() {
        return (
            <div className="homeBody">
                <Grid container xs={12} className="pLeftRight15">
                    <Grid container xs={12} justify="center">
                        <h1 className="h1Text">TESTIMONIALS</h1>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_1}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_2}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_3}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_4}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_5}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_6}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_7}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_8}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_9}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_10}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_11}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_12}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_13}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_14}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_15}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_16}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_17}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_18}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_19}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_20}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{TestimonialsContent.testimonial_21}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{TestimonialsContent.testimonial_22}</p>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(Testimonials);