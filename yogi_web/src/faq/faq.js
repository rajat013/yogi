import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withRouter } from "react-router-dom";

import { FaqContent } from '../content/content'
import '../home/home.css';

class Faq extends React.Component {

    state = {

    };

    componentDidMount() {
        window.scroll(0, 0);
    }

    goToPath = (path) => {
        this.props.history.push(path);
    }

    render() {
        return (
            <div className="homeBody">
                <Grid container xs={12} className="pLeftRight15">
                    <Grid container xs={12} justify="center">
                        <h1 className="h1Text">FAQ</h1>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_1}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_2}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_3}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_4}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_5}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_6}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_7}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_8}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_9}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_10}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_11}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_12}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_13}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_14}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_15}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_16}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_17}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_18}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_19}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_20}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_21}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_22}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_23}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_24}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_25}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_26}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_27}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_28}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_29}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_30}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_31}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_32}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_33}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_34}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_35}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_36}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_37}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_38}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText yellowColor">{FaqContent.faq_39}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{FaqContent.faq_40}</p>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(Faq);