import React from 'react';
import Grid from '@material-ui/core/Grid';
import MenuIcon from '@material-ui/icons/Menu';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import { withRouter } from "react-router-dom";

import MenuList from '../menuList/menuList'

import './header.css';
import { EnvProp } from '../services/envProperties';

class Header extends React.Component {

    state = {
        yogiLogoUrl: EnvProp.webImgUrl + 'yogi_logo.png',
        right: false
    }

    toggleDrawer = (open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        this.setState({ right: open });
    };

    closeDrawer = () => {
        this.setState({ right: false });
    }

    openSideMenu = () => {
        this.setState({ right: true });
    }

    goToHome = () => {
        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <div>
                    <React.Fragment>
                        <SwipeableDrawer
                            anchor={'right'}
                            open={this.state.right}
                            onClose={this.toggleDrawer(false)}
                            onOpen={this.toggleDrawer(true)}
                        >
                            <MenuList closeMenu={this.closeDrawer} />
                        </SwipeableDrawer>
                    </React.Fragment>
                </div>
                <Grid container xs={12}>
                    <Grid container xs={10} justify="center" className="headerBottom" onClick={() => this.goToHome()}>
                        <img alt='' className="yogiMobileLogo" src={this.state.yogiLogoUrl} />
                    </Grid>
                    <Grid xs={2} container className="headerMarTop" onClick={this.toggleDrawer(true)}>
                        <MenuIcon className="hambMarLeft" />
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(Header);