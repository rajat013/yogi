import React from 'react';
import Grid from '@material-ui/core/Grid';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import TextField from '@material-ui/core/TextField';
import { withRouter } from "react-router-dom";

import { EnvProp, Headers } from '../services/envProperties';
import './contact.css';

class Contact extends React.Component {

    state = {
        name: '',
        nameErr: false,
        email: '',
        emailErr: '',
        mobile: '',
        mobileErr: '',
        comments: '',
        commentErr: '',
        alertOpen: false,
        alertMsg: '',
        wordsAllowed: 1200,
        wordsRem: 1200,
        errMsg: ''
    };

    nameField = (event) => {
        let tempName = event.target.value
        this.setState({ name: tempName });
    }

    emailField = (event) => {
        this.setState({ email: event.target.value });
    }

    mobileField = (event) => {
        this.setState({ mobile: event.target.value });
    }

    commentsField = (e) => {
        if (e.target.value.length <= this.state.wordsAllowed) {
            this.setState({ comments: e.target.value, wordsRem: (this.state.wordsAllowed - e.target.value.length) })
        }
        else {
            alert('Maximum word limit reached');
        }
    }

    handleAlertClose = () => {
        this.setState({ alertOpen: false });
    }

    submitContact = async () => {
        try {
            //let emailCheck = false;
            if (100 < this.state.name.length) {
                this.setState({ alertOpen: true, nameErr: true, errMsg: 'Maximum 100 words allowed for Name' });
            }
            else if ('' === this.state.name) {
                this.setState({ alertOpen: true, nameErr: true, errMsg: 'Name can not be blank' });
            }
            else if (100 < this.state.email.length) {
                this.setState({ alertOpen: true, emailErr: true, errMsg: 'Maximum 100 words allowed for Email' });
            }
            else if ('' === this.state.email) {
                this.setState({ alertOpen: true, emailErr: true, errMsg: 'Email can not be blank' });
            }
            // else if(!emailCheck){

            // }
            else if (10 < this.state.mobile.length || 10 > this.state.mobile.length) {
                this.setState({ alertOpen: true, mobileErr: true, errMsg: 'Mobile no must be 10 digits' });
            }
            else if ('' === this.state.mobile) {
                this.setState({ alertOpen: true, mobileErr: true, errMsg: 'Phone no can not be blank' });
            }
            else if (1200 < this.state.comments.length) {
                this.setState({ alertOpen: true, commentErr: true, errMsg: 'Maximum 1200 words allowed for Comments' });
            }
            else if ('' === this.state.comments) {
                this.setState({ alertOpen: true, commentErr: true, errMsg: 'Comments can not be blank' });
            }
            else {
                this.setState({ nameErr: false });
                let obj = {
                    'name': this.state.name,
                    'email': this.state.email,
                    'mobile': this.state.mobile,
                    'comments': this.state.comments
                }
                let config = Headers(obj);
                let response = await fetch(EnvProp.baseUrl + '/submitContact', config);
                let data = await response.json();
                alert(data);
                window.location.reload();
            }
        }
        catch (err) {
            console.log(err);
        }
    }
    render() {
        return (
            <div className="homeBody">
                <Grid container xs={12}>
                    <Grid container xs={12} justify="center">
                        <h1>CONTACT</h1>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText mLeft15">For faster service, please complete this form. We are available to photograph your portrait or event anywhere in the world.</p>
                    </Grid>
                    <Grid container xs={12} className="textFieldMBot mTop30" justify="center">
                        <TextField id="standard-basic" error={this.state.nameErr} label="Name" required variant="outlined"
                            className="textFieldBody" onChange={this.nameField} />
                    </Grid>
                    <Grid container xs={12} className="textFieldMBot" justify="center">
                        <TextField id="standard-basic" label="Email" error={this.state.emailErr} required variant="outlined"
                            className="textFieldBody" onChange={this.emailField} />
                    </Grid>
                    <Grid container xs={12} className="textFieldMBot" justify="center">
                        <TextField id="standard-basic" label="Mobile Number" error={this.state.mobileErr} type='number' className="textFieldBody"
                            required variant="outlined" onChange={this.mobileField} />
                    </Grid>
                    <Grid container xs={12} className="textFieldMBot" justify="center">
                        <TextField id="outlined-multiline-static" multiline error={this.state.commentErr} rows={4} label="Comments" required variant="outlined"
                            className="textFieldBody" onChange={this.commentsField} />
                    </Grid>
                    <Grid container xs={12} className="wordsRem">
                        {this.state.wordsRem} words Remaining
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <div className="contactBtn hand" onClick={this.submitContact}>
                            Submit
                        </div>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <Snackbar open={this.state.alertOpen} autoHideDuration={3000} onClose={() => this.handleAlertClose()}>
                            <SnackbarContent message={this.state.errMsg} style={{ backgroundColor: '#808080' }} />
                        </Snackbar>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(Contact);