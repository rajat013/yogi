import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withRouter } from "react-router-dom";

import { PromiseContent } from '../content/content'
import '../home/home.css';

class Promise extends React.Component {

    state = {

    };

    componentDidMount() {
        window.scroll(0, 0);
    }

    goToPath = (path) => {
        this.props.history.push(path);
    }

    render() {
        return (
            <div className="homeBody">
                <Grid container xs={12} className="pLeftRight15">
                    <Grid container xs={12} justify="center">
                        <h1 className="h1Text">WEDDINGS</h1>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{PromiseContent.promise_1}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{PromiseContent.promise_2}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <span className="borderLine"></span>
                    </Grid>
                    <Grid container xs={12} justify="center" className="contentSubHeader">
                        <h2>MY PROMISE</h2>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{PromiseContent.promise_3}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{PromiseContent.promise_4}</p>
                    </Grid>
                    <Grid container xs={12} justify="center">
                        <p className="contentText">{PromiseContent.promise_5}</p>
                    </Grid>
                </Grid>
            </div>
        )
    }
}

export default withRouter(Promise);