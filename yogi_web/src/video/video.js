import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withRouter } from "react-router-dom";

//import { EnvProp } from '../services/envProperties';
import './video.css';

class Video extends React.Component {

    state = {

    };

    videoList = [
        'https://www.youtube.com/embed/GYtmTZQ9eYg',
        'https://www.youtube.com/embed/Yshdfe3zNrA',
        'https://www.youtube.com/embed/Yshdfe3zNrA',
        'https://www.youtube.com/embed/GYtmTZQ9eYg'
    ];

    render() {
        return (
            <div className="homeBody">
                <Grid container md={12} justify="center">
                    {this.videoList.map((e) => {
                        return (
                            <div className="videoBody">
                                <iframe title="DashVideo" width="400" height="300" allowFullScreen="allowfullscreen"
                                    mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen"
                                    oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"
                                    frameBorder="0" src={e}>
                                </iframe>
                            </div>
                        )
                    })}
                </Grid>
            </div>
        )
    }
}

export default withRouter(Video);