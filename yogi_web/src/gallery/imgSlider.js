import React from 'react';
import Grid from '@material-ui/core/Grid';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { withRouter } from "react-router-dom";

import { PreWeddingList, WeddingList } from '../tiles/tiles';

class ImgSlider extends React.Component {

    state = {
        slideImagePath: '',
        imgIdx: ''
    };

    componentDidMount() {
        if (typeof this.props.imgIndex !== 'undefined') {

        }
    }

    nextSlide = () => {
        console.log('sdfsd');
        let tileList = [];
        if ('/preWedding' === this.props.history.location.pathname) {
            tileList = [...PreWeddingList];
        }
        else if ('/wedding' === this.props.history.location.pathname) {
            tileList = [...WeddingList];
        }
        if (this.state.imgIdx < (tileList.length + 1)) {
            let imgIdx = this.state.imgIdx;
            this.setState({ slideImagePath: tileList[imgIdx + 1].img, imgIdx: (imgIdx + 1) });
        }
    }

    prevSlide = () => {
        let tileList = [];
        if ('/preWedding' === this.props.history.location.pathname) {
            tileList = [...PreWeddingList];
        }
        else if ('/wedding' === this.props.history.location.pathname) {
            tileList = [...WeddingList];
        }
        if (0 <= this.state.imgIdx) {
            let imgIdx = this.state.imgIdx;
            this.setState({ slideImagePath: tileList[imgIdx - 1].img, imgIdx: (imgIdx - 1) });
        }
    }

    handleKeyDown = (e) => {
        console.log('Enter key pressed');
        if (e.keyCode === 37) {
            this.prevSlide();
        } else if (e.keyCode === 39) {
            this.nextSlide();
        }
    }

    render() {
        return (
            <div>
                <Grid container xs={1} justify="center" className="hand" onClick={() => this.prevSlide()}>
                    <div className="arrowContainer">
                        <span className="verticalCenter">
                            <ArrowBackIosIcon />
                        </span>
                    </div>
                </Grid>
                <Grid container xs={10} justify="center">
                    <div>
                        <img src={this.state.slideImagePath} className="slideImageBody" alt='' id="demo" />
                    </div>
                </Grid>
                <Grid container xs={1} justify="center" className="hand" onClick={() => this.nextSlide()}>
                    <div className="arrowContainer">
                        <span className="verticalCenter">
                            <ArrowForwardIosIcon />
                        </span>
                    </div>
                </Grid>
            </div>
        )
    }
}

export default withRouter(ImgSlider);