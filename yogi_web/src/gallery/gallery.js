import React from 'react';
import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { withRouter } from "react-router-dom";

//import { EnvProp } from '../services/envProperties';
import { PreWeddingList, WeddingList } from '../tiles/tiles';
import './gallery.css';

class Gallery extends React.Component {

    state = {
        imgColOne: [],
        imgColTwo: [],
        imgColThree: [],
        showSlide: false,
        slideImagePath: '',
        imgIdx: '',
        touchStart: '',
        touchEnd: ''
    };

    componentDidMount() {
        window.scroll(0, 0);
        let tempImgListOne = [];
        let tempImgListTwo = [];
        let tempImgListThree = [];
        let count = 1;
        let tileList = [];
        if ('/preWedding' === this.props.history.location.pathname) {
            tileList = [...PreWeddingList];
        }
        else if ('/wedding' === this.props.history.location.pathname) {
            tileList = [...WeddingList];
        }
        else {
            this.props.history.push('/');
        }
        for (let i = 0; i < tileList.length; i++) {
            // eslint-disable-next-line
            if (count <= (tileList.length / 3)) {
                tempImgListOne.push(tileList[i]);
            }
            if (count > (tileList.length / 3) && count <= (tileList.length / 1.5)) {
                tempImgListTwo.push(tileList[i]);
            }
            if (count > (tileList.length / 1.5) && count <= (tileList.length)) {
                tempImgListThree.push(tileList[i]);
            }
            if (i === (tileList.length - 1)) {
                this.setState({ imgColOne: tempImgListOne, imgColTwo: tempImgListTwo, imgColThree: tempImgListThree });
            }
            count++;
        }
    }

    componentDidUpdate() {
        window.scroll(0, 0);
    }

    openSlide = (e) => {
        let tileList = [];
        if ('/preWedding' === this.props.history.location.pathname) {
            tileList = [...PreWeddingList];
        }
        else if ('/wedding' === this.props.history.location.pathname) {
            tileList = [...WeddingList];
        }
        tileList.forEach((i, idx) => {
            if (i.img === e.img) {
                this.setState({ showSlide: true, slideImagePath: e.img, imgIdx: idx });
            }
        })
    }

    closeSlide = () => {
        this.setState({ showSlide: false, slideImagePath: '' });
    }

    nextSlide = () => {
        console.log('sdfsd');
        let tileList = [];
        if ('/preWedding' === this.props.history.location.pathname) {
            tileList = [...PreWeddingList];
        }
        else if ('/wedding' === this.props.history.location.pathname) {
            tileList = [...WeddingList];
        }
        if (this.state.imgIdx < (tileList.length + 1)) {
            let imgIdx = this.state.imgIdx;
            this.setState({ slideImagePath: tileList[imgIdx + 1].img, imgIdx: (imgIdx + 1) });
        }
    }

    prevSlide = () => {
        let tileList = [];
        if ('/preWedding' === this.props.history.location.pathname) {
            tileList = [...PreWeddingList];
        }
        else if ('/wedding' === this.props.history.location.pathname) {
            tileList = [...WeddingList];
        }
        if (0 <= this.state.imgIdx) {
            let imgIdx = this.state.imgIdx;
            this.setState({ slideImagePath: tileList[imgIdx - 1].img, imgIdx: (imgIdx - 1) });
        }
    }

    handleKeyDown = (e) => {
        if (e.keyCode === 37) {
            this.prevSlide();
        } else if (e.keyCode === 39) {
            this.nextSlide();
        }
    }

    alwaysFocus = (e) => {
        if (this.state.showSlide) {
            let elm = e.target;
            setTimeout(function () {
                elm.focus();
                window.scroll(0, 0);
            });
        }
    }

    handleTouchStart = (e) => {
        this.setState({ touchStart: e.targetTouches[0].clientX });
    }

    handleTouchMove = (e) => {
        this.setState({ touchEnd: e.targetTouches[0].clientX });
    }

    handleTouchEnd = () => {
        if (this.state.touchStart - this.state.touchEnd > 100) {
            // do your stuff here for left swipe
            this.nextSlide();
        }

        if (this.state.touchStart - this.state.touchEnd < -100) {
            // do your stuff here for right swipe
            this.prevSlide();
        }
    }

    render() {
        return (
            <div className="homeBody">
                <Grid container xs={12} className={(this.state.showSlide) ? 'displayNone' : 'displayFlex'}>
                    <Grid container xs={12} sm={6} md={4}>
                        <div>
                            {this.state.imgColOne.map((e, idx) => {
                                return (
                                    <div>
                                        <img src={e.img} className={('P' === e.type) ? 'imgGrid mBot10' : 'imgGridLandscape mBot10'}
                                            onClick={() => this.openSlide(e)} alt='' />
                                    </div>
                                )
                            })}
                        </div>
                    </Grid>
                    <Grid container xs={12} sm={6} md={4}>
                        <div>
                            {this.state.imgColTwo.map((e) => {
                                return (
                                    <Grid container xs={12} justify="center">
                                        <img src={e.img} className={('P' === e.type) ? 'imgGridTwo mBot14' : 'imgGridLandscapeTwo mBot14'}
                                            onClick={() => this.openSlide(e)} alt='' />
                                    </Grid>
                                )
                            })}
                        </div>
                    </Grid>
                    <Grid container xs={12} sm={6} md={4}>
                        <div>
                            {this.state.imgColThree.map((e) => {
                                return (
                                    <Grid container xs={12} justify="center">
                                        <img src={e.img} className={('P' === e.type) ? 'imgGrid' : 'imgGridLandscape'}
                                            onClick={() => this.openSlide(e)} alt='' />
                                    </Grid>
                                )
                            })}
                        </div>
                    </Grid>
                </Grid>
                <Hidden mdDown>
                    <Grid container xs={12} hidden={!this.state.showSlide}>
                        <Grid container xs={1} justify="center" className="hand" onClick={() => this.prevSlide()}>
                            <div className="arrowContainer" hidden={!this.state.showSlide}>
                                <span className="verticalCenter">
                                    <ArrowBackIosIcon />
                                </span>
                            </div>
                        </Grid>
                        <Grid container xs={10} justify="center">
                            <div hidden={!this.state.showSlide}>
                                <img src={this.state.slideImagePath} className="slideImageBody" alt='' id="demo" />
                            </div>
                        </Grid>
                        <Grid container xs={1} justify="center" className="hand" onClick={() => this.nextSlide()}>
                            <div className="arrowContainer" hidden={!this.state.showSlide}>
                                <span className="verticalCenter">
                                    <ArrowForwardIosIcon />
                                </span>
                            </div>
                        </Grid>
                        <Grid container xs={12} justify="center">
                            <div className="closeSlide hand" onClick={() => this.closeSlide()} hidden={!this.state.showSlide}>
                                <HighlightOffIcon />
                            </div>
                            <input className="phantomInput" type="text" ref={input => input && input.focus()}
                                onKeyUp={(event) => this.handleKeyDown(event)} autoFocus onBlur={(event) => this.alwaysFocus(event)} />
                        </Grid>
                    </Grid>
                </Hidden>
                <Hidden lgUp>
                    <div id="main">
                        <Grid container xs={12} hidden={!this.state.showSlide}>
                            <Grid container xs={12} justify="center">
                                <div hidden={!this.state.showSlide}>
                                    <img src={this.state.slideImagePath} className="slideImageBodyMobile imgBoxShadow" alt=''
                                        onTouchStart={touchStartEvent => this.handleTouchStart(touchStartEvent)}
                                        onTouchMove={touchMoveEvent => this.handleTouchMove(touchMoveEvent)}
                                        onTouchEnd={() => this.handleTouchEnd()} />
                                </div>
                            </Grid>
                            <Grid container xs={12} justify="center">
                                <div className="closeSlide hand" onClick={() => this.closeSlide()} hidden={!this.state.showSlide}>
                                    <HighlightOffIcon />
                                </div>
                            </Grid>
                        </Grid>
                    </div>
                </Hidden>
            </div>
        )
    }
}

export default withRouter(Gallery);