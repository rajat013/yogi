import React from 'react';
import { withRouter } from "react-router-dom";

import Gallery from './gallery';
import './gallery.css';

class PreWedding extends React.Component {

    state = {
        imgColOne: [],
        imgColTwo: [],
        imgColThree: []
    };

    componentDidMount() {
        
    }

    render() {
        return (
            <Gallery />
        )
    }
}

export default withRouter(PreWedding);