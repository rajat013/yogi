import React from 'react';
import Grid from '@material-ui/core/Grid';
import { withRouter } from "react-router-dom";

//import { EnvProp } from '../services/envProperties';
import './footer.css';

class Footer extends React.Component {
    state = {

    }

    goToContact = () => {
        console.log('goToContact');
        this.props.history.push('/contact');
    }

    render() {
        return (
            <Grid container xs={12}>
                <Grid className={('/contact' === this.props.history.location.pathname) ? 'displayNone' : 'placebo'} container xs={12} justify="center">
                    <div className="contactBtn hand" onClick={() => this.goToContact()}>
                        Contact Yogi PhotoArt
                    </div>
                </Grid>
                <Grid container xs={12} justify="center">
                    <span className="footerSignature">SIGNATURE WEDDING PHOTOGRAPHER</span>
                </Grid>
                <Grid container xs={12} justify="center">
                    <span className="yogiColor">–YOGI PHOTOART–</span>
                </Grid>
                <Grid container xs={12} justify="center">
                    <span className="footerText">Engagements • Preweddings • Weddings</span>
                </Grid>
                <Grid container xs={12} justify="center">
                    <span className="footerText mBot50">(+91) 955 255 7989 •hello@yogiphotoart.com • © 2020</span>
                </Grid>
            </Grid>
        )
    }
}

export default withRouter(Footer);