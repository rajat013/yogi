import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Grid from '@material-ui/core/Grid';
import Hidden from '@material-ui/core/Hidden';

import Header from '../header/header';
import Home from '../home/home';
import Contact from '../contact/contact';
import Gallery from '../gallery/gallery';
import Video from '../video/video';
import AboutUs from '../aboutUs/aboutUs';
import Promise from '../promise/promise';
import Faq from '../faq/faq';
import Testimonials from '../testimonials/testimonials';
//import Chat from '../chat/chat';
import MenuListDesktop from '../menuList/menuListDesktop';
import Footer from '../footer/footer';
import PreWedding from '../gallery/preWedding';
import Wedding from '../gallery/wedding';

import '../chat/chat.css'
import '../home/home.css'
//import PrivateRoute from '../services/protectedRoute';
//import { EnvProp, Headers } from '../services/envProperties';

class Router extends React.Component {

    state = {};

    componentDidMount() {

    }

    render() {
        return (
            <div>
                <BrowserRouter>
                    <Hidden lgUp>
                        <div>
                            <Header />
                        </div>
                    </Hidden>
                    {/* <div className="chatIconDiv">
                        <Chat />
                    </div> */}
                    <Switch>
                        <Grid container xs={12}>
                            <Hidden mdDown>
                                <Grid container xs={2} xl={2}>
                                    <MenuListDesktop />
                                </Grid>
                            </Hidden>
                            <Grid container xs={12} sm={12} md={12} lg={10} xl={10}>
                                <Route path="/" exact component={Home} />
                                <Switch>
                                    <Route path="/video" exact component={Video} />
                                    <Route path="/contact" exact component={Contact} />
                                    <Route path="/gallery" exact component={Gallery} />
                                    <Route path="/preWedding" exact component={PreWedding} />
                                    <Route path="/wedding" exact component={Wedding} />
                                    <Route path="/aboutUs" exact component={AboutUs} />
                                    <Route path="/myPromise" exact component={Promise} />
                                    <Route path="/help" exact component={Faq} />
                                    <Route path="/testimonials" exact component={Testimonials} />
                                </Switch>
                                <Footer />
                            </Grid>
                        </Grid>
                    </Switch>
                </BrowserRouter>
            </div>
        )
    }
}

export default Router;