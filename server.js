//const https = require('https');
const http = require('http');
const fs = require('fs');
const app = require('./yogi_app/app');
const mongoose = require('./db');

/*const options = {
    key: fs.readFileSync('server.key'),
    cert: fs.readFileSync('server.crt'),
    passphrase: 'rascal',
    requestCert: true
};*/

const port = process.env.PORT || 4000;
app.set('port', port);
//const server = https.createServer(options, app);
const server = http.createServer(app);
console.log('server started');
mongoose.connect_db();
console.log('connected to DB');

server.listen(port);