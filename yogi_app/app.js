const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const router = express.Router();
const path = require('path');
const telegram = require('node-telegram-bot-api');

const log = require('./logs/logger');
const contact = require('./contact/contact_controller');
const go_daddy = require('./contact/go_daddy_mail');


const get_line_no = () => {
    var obj = {};
    Error.captureStackTrace(obj, get_line_no);
    return obj.stack;
};

const app = express();
app.use(cors());

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

app.use(bodyParser());

app.use(express.static(path.join(__dirname, '../yogi_web/build')))
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, '../yogi_web/build', 'index.html'));
})
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, '../yogi_web/build', 'index.html'))
});

const telegramToken = '1562164487:AAH2nZitctYdVJTVSOKLOJGJ-Aevr_hS_mY';

let tempMsg = ''

app.post('/api/testIt', (req, res, next) => {
    const telegramBot = new telegram(telegramToken, { polling: true });
    try {
        const chat_id = 1017610462;
        telegramBot.sendMessage(chat_id, req.body.msg);

        telegramBot.on('message', (msg) => {
            let user_chat_id = msg.chat.id;
            tempMsg = msg.text;
            console.log(msg.text);
        });
        res.json('success')
    } catch (err) {
        log.error(err, get_line_no());
        console.log(err);
    }
});

app.post('/api/pingCheck', (req, res, next) => {
    try {
        res.json(tempMsg);
        tempMsg = '';
    } catch (err) {
        log.error(err, get_line_no());
        console.log(err);
    }
});

app.post('/api/submitContact', (req, res, next) => {
    try {
        go_daddy.send_confirmation_mail(req, res);
        contact.save_contact_data(req,res);
    } catch (err) {
        log.error(err, get_line_no());
        console.log(err);
    }
});

module.exports = app;