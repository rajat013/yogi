const log4js = require('log4js');
const express = require('express');

const log = log4js.getLogger('yogi');

module.exports.info = (data) => {
    log4js.configure({
        appenders: { yogi: { type: 'file', filename: 'yogi.log' } },
        categories: { default: { appenders: ['yogi'], level: 'info'} }
      });
    log.info(`${data}`);
}

module.exports.error = (data,line_no) => {
    log4js.configure({
        appenders: { yogi: { type: 'file', filename: 'yogi.log' } },
        categories: { default: { appenders: ['yogi'], level: 'error' } }
      });
    console.log(data,line_no)
    log.error(data,line_no);
}