const log = require('../logs/logger');
const contact_model = require('./contact_model');

const get_line_no = () => {
    let obj = {};
    Error.captureStackTrace(obj, get_line_no);
    return obj.stack;
};

module.exports.save_contact_data = (req, res) => {

    try {
        let save_contact_model = contact_model.contact_data();
        let new_model = {
            r_name: req.body.name,
            r_email: req.body.email,
            r_mobile: req.body.mobile,
            r_comments: req.body.comments,
            r_created_date: new Date()
        }
        let obj = new save_contact_model(new_model);
        obj.save((err, result) => {
            if (!err) {
                log.info(`${req.body.name} stock added}`);
                console.log(result);
                res.json('Details have been submitted');
            }
            else {
                log.error(err, get_line_no());
            }
        })
    }
    catch (err) {
        log.error(err, get_line_no());
    }
}