const mongoose = require('mongoose');

let contact_schema = new mongoose.Schema({
    r_name: String,
    r_email: String,
    r_mobile: Number,
    r_comments: String,
    r_created_date: Date
});
const contact_model = mongoose.model('t_contacts', contact_schema);
const contact_data = () => {
    return contact_model;
}
exports.contact_data = contact_data;
