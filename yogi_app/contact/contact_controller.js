const nodemailer = require('nodemailer');
const { google } = require('googleapis');
const OAuth2 = google.auth.OAuth2;
const log = require('../logs/logger');
const contact_dao = require('./contact_dao');

const get_line_no = () => {
    let obj = {};
    Error.captureStackTrace(obj, get_line_no);
    return obj.stack;
};

let accessToken = null;

const oauth2Client = new OAuth2(
    '728876006584-nbo2gg8e1l6i8nja0int2e3n6kejdtac.apps.googleusercontent.com',
    's04SW1Vo1pIQLtvC30c9yVSO', // Client Secret
    'https://developers.google.com/oauthplayground' // Redirect URL
);

module.exports.send_confirmation_mail = function (req, res) {
    //res.json({ status: true, new_user: false });
    try {
        const refresh_acc_token = () => {
            oauth2Client.setCredentials({
                refresh_token: '1//044U32uyXYE6pCgYIARAAGAQSNwF-L9IrIFfIEJwwQB-Q5Ev6JcZBfxcPh4MX-1GmLCmv7mc8EntCN4Ah6j_XTL7xIGxVx34tepE'
            });
            accessToken = oauth2Client.getAccessToken();
        }
        refresh_acc_token();
        const smtpTransport = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                type: 'OAuth2',
                user: 'support@eqty.in',
                clientId: '728876006584-nbo2gg8e1l6i8nja0int2e3n6kejdtac.apps.googleusercontent.com',
                clientSecret: 's04SW1Vo1pIQLtvC30c9yVSO',
                refreshToken: '1//044U32uyXYE6pCgYIARAAGAQSNwF-L9IrIFfIEJwwQB-Q5Ev6JcZBfxcPh4MX-1GmLCmv7mc8EntCN4Ah6j_XTL7xIGxVx34tepE',
                accessToken: accessToken
            }
        });
        let user_mail = {
            from: 'YogiPhotoArt.com <eqty@eqty.in>',
            to: req.body.email,
            //to: req.email,
            subject: 'Yogi Photo Art',
            text: `Greetings - ${req.body.name},\r\n\r\nWe thank you for contacting us, our representative will connect with you shortly\r\n\r\nThank you\r\nRegards\r\nYogi Photo Art`,
            //generateTextFromHTML: false,
            //html: '<b>Test</b>'
        };
        smtpTransport.sendMail(user_mail, (error, response) => {
            if (!error) {
                console.log(accessToken);
                smtpTransport.close();
            }
            else {
                console.log(error);
                
            }
        });

        let admin_mail = {
            from: 'YogiPhotoArt.com <eqty@eqty.in>',
            to: 'nsmaske@gmail.com',
            //to: req.email,
            subject: 'New User contacted',
            text: `name - ${req.body.name}, email - ${req.body.email}, mobile - ${req.body.mobile}`,
            //generateTextFromHTML: false,
            //html: '<b>Test</b>'
        };
        smtpTransport.sendMail(admin_mail, (error, response) => {
            if (!error) {
                console.log(accessToken);
                smtpTransport.close();
            }
            else {
                console.log(error);
                
            }
        });
    }
    catch (err) {
        console.log(err);
    }
}

module.exports.save_contact_data = (req, res) => {
    try{
        contact_dao.save_contact_data(req, res);
    }
    catch (err) {
        res.json(`Some error occurred`);
        log.error(err, get_line_no());
    }
}